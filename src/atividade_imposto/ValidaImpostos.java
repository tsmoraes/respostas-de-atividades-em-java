package atividade_imposto;

/**O m�todo que recebe apenas a String imposto veriica se o conte�do da
 *  String � �INSS� ou �IR�. Se for o primeiro, retorna �7�, se for o 
 *  segundo retorna �23�. O m�todo que recebe um valor e mais a String 
 *  imposto calcula o valor de imposto pelo valor recebido.
 * 
 *  @author Thiago da Silva
 */

public class ValidaImpostos {
	double resp;

	public double calculaImpostos(double valor, String imposto) {
		
		switch (imposto) {
		case "INSS":
			resp = (valor * 7)/100;
			break;
		case "IR":
			resp = (valor * 23)/100;
		}
		return resp;
	}

	public double calculaImpostos(String imposto) {
		switch (imposto) {
		case "INSS":
			resp = 7;
			break;
		case "IR":
			resp = 23;
			break;
		default:
			System.out.println("Somente � aceito IR ou INSS. Grato!");
		}
		return resp;
	}
}
