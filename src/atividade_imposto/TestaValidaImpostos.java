package atividade_imposto;

/**Informa o valor do imposto e a porcentagem.
 * 
 *  @author Thiago da Silva
 */

import javax.swing.JOptionPane;

public class TestaValidaImpostos {

	public static void main(String[] args) {
		double v, calc;
		String i;
		
		// testando o primeiro metodo
		i = JOptionPane.showInputDialog("Qual a porcentagem gostaria de saber IR ou INSS? ");

		ValidaImpostos e1 = new ValidaImpostos();
		calc = e1.calculaImpostos(i);
		JOptionPane.showMessageDialog(null, "A porcentagem do " + i + " � de: " + calc + "%");

		// testando o primeiro segundo metodo
		v = Double.parseDouble(JOptionPane.showInputDialog("Sobre qual rendimento? "));
		i = JOptionPane.showInputDialog("ser� sobre IR ou INSS?: ");

		ValidaImpostos e2 = new ValidaImpostos();
		calc = e2.calculaImpostos(v, i);
		JOptionPane.showMessageDialog(null, "O valor do seu Imposto de Renda � de: " + calc);

	}

}
