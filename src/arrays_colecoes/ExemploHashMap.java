/**
 * 
 */
package arrays_colecoes;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 * @author Thiago
 *
 */
public class ExemploHashMap {
	private static String[] nomes = { "Jo�o", "Jos�", "Pedro", "Carlos", "Jaqueline", "Matheus", "Marcelo" };

	/**
	 * 
	 */
	@SuppressWarnings("deprecation")
	public ExemploHashMap() {
		HashMap map = new HashMap();
		Integer i;

		for (int j = 0; j < nomes.length; j++) {
			i = (Integer) map.get(new Character(nomes[j].charAt(0)));

			if (i == null) {
				map.put(new Character(nomes[j].charAt(0)), new Integer(1));
			} else {
				map.put(new Character(nomes[j].charAt(0)), new Integer(i.intValue() + 1));

			}
		}

		this.geraSaidaMap(map);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ExemploHashMap();

	}

	private void geraSaidaMap(Map mapRef) {
		String saida = "";
		saida += "N�mero de palavras iniciadas com a letra:";
		saida += "\n" + mapRef.toString();
		saida += "\nQuantidade de valores do Map: " + mapRef.size();
		saida += "\n� vazio?: " + mapRef.isEmpty();
		JTextArea saidaArea = new JTextArea();
		saidaArea.setText(saida);

		JOptionPane.showMessageDialog(null, saidaArea, "Trabalhando com Maps", JOptionPane.INFORMATION_MESSAGE);

		System.exit(0);
	}

}
