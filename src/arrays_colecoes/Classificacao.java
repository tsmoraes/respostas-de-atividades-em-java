/**
 * 
 */
package arrays_colecoes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 * @author Thiago
 *
 */
public class Classificacao {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> listaNomes = new ArrayList<String>();
		listaNomes.add("Maria");
		listaNomes.add("Jo�o");
		listaNomes.add("Jos�");
		listaNomes.add("Adriano");
		listaNomes.add("Willian");
		listaNomes.add("Patr�cia");

		String saida = "";
		saida += "Lista desordenada: " + listaNomes;

		Collections.sort(listaNomes);

		saida += "\nLista ordenada: " + listaNomes;

		JTextArea saidaArea = new JTextArea();
		saidaArea.setText(saida);

		JOptionPane.showMessageDialog(null, saidaArea, "Trabalhando com ArrayList", JOptionPane.INFORMATION_MESSAGE);

		System.exit(0);
	}

}
