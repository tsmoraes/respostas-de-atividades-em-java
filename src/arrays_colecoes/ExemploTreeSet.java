/**
 * 
 */
package arrays_colecoes;

import java.util.Arrays;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 * @author Thiago
 *
 */
public class ExemploTreeSet {
	String saida = "";
	private String[] cores = { "vermelho", "branco", "azul", "verde", "cinza", "laranja", "bronzeado", "branco",
			"ciano", "p�ssego", "cinza", "laranja" };

	/**
	 * 
	 */
	public ExemploTreeSet() {
		TreeSet tree = new TreeSet(Arrays.asList(cores));

		saida += "\nSet de elementos n�o " + "duplicados e ordenados:\n";
		this.mostrarSet(tree);

		saida += "\n\nSubconjunto de TreeSet " + "menor que �laranja�:\n";
		this.mostrarSet(tree.headSet("laranja"));

		saida += "\n\nSubconjunto de TreeSet " + "maior que ou igual a �laranja�:\n";
		this.mostrarSet(tree.tailSet("laranja"));

		saida += "\n\nPrimeiro elemento de set: " + tree.first();
		saida += "\n�ltimo elemento de set: " + tree.last();

		JTextArea saidaArea = new JTextArea();
		saidaArea.setText(saida);

		JOptionPane.showMessageDialog(null, saidaArea, "Trabalhando com Sets", JOptionPane.INFORMATION_MESSAGE);

		System.exit(0);
	}

	private void mostrarSet(SortedSet tail) {
		 Iterator iterator = tail.iterator(); 
		 
		 while (iterator.hasNext()) {
			 saida += iterator.next() + " "; 
		 }
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ExemploTreeSet();
	}

}
