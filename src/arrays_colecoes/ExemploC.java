/**
 * 
 */
package arrays_colecoes;

import java.awt.Container;

import javax.swing.JApplet;
import javax.swing.JTextArea;

/**
 * @author Thiago da Silva
 *
 */
@SuppressWarnings({ "deprecation", "serial" })
public class ExemploC extends JApplet {
	JTextArea saidaArea;

	@Override
	public void init() {
		super.init();

		saidaArea = new JTextArea();
		Container container = getContentPane();
		container.add(saidaArea);

		int[][] array1 = { { 1, 2, 3 }, { 4, 5, 6 } };
		int[][] array2 = { { 1, 2 }, { 3 }, { 4, 5, 6 } };

		saidaArea.setText("Os valores do array1 s�o\n");
		buildSaida(array1);

		saidaArea.append("Os valores do array2 s�o\n");
		buildSaida(array2);
	}

	public void buildSaida(int[][] array) {
		
		// percorre as linhas do array com um for
		for (int linha = 0; linha < array.length; linha++) {
			
			// percorre as colunas da linha corrente com outro 30for
			for (int coluna = 0; coluna < array[linha].length; coluna++) {
				saidaArea.append(array[linha][coluna] + " ");
			}
			saidaArea.append("\n");
		}
		saidaArea.append("\n");
	}

}
