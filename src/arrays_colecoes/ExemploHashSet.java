/**
 * 
 */
package arrays_colecoes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 * @author Thiago
 *
 */
public class ExemploHashSet {
	String saida = "";
	private String[] cores = { "vermelho", "branco", "azul", "verde", "cinza", "laranja", "bronzeado", "branco",
			"ciano", "p�ssego", "cinza", "laranja" };

	/**
	* 
	*/
	public ExemploHashSet() {
		List lista;
		lista = new ArrayList(Arrays.asList(cores));
		saida += "Lista com elementos duplicados: ";
		saida += "\n" + lista;

		this.gerarListaNaoDuplicada(lista);

		JTextArea saidaArea = new JTextArea();
		saidaArea.setText(saida);

		JOptionPane.showMessageDialog(null, saidaArea, "Trabalhando com Sets", JOptionPane.INFORMATION_MESSAGE);

		System.exit(0);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ExemploHashSet();

	}

	public void gerarListaNaoDuplicada(Collection collection) {
		Set set = new HashSet(collection);
		Iterator iterator = set.iterator();

		saida += "\n\nLista com elementos n�o duplicados\n";

		while (iterator.hasNext()) {
			saida += iterator.next() + " ";
		}
	}
}
