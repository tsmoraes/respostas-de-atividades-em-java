/**
 * 
 */
package arrays_colecoes;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 * @author Thiago
 *
 */
public class ExemploArrayList {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		List<String> listaFrutas = new ArrayList<String>();
		String s = "uva";
		listaFrutas.add("melancia");
		listaFrutas.add(s);
		listaFrutas.add("caju");

		String saida = "";
		saida += "Frutas da Lista: " + listaFrutas.toString();
		saida += "\nTotal de Frutas na lista: " + listaFrutas.size();
		saida += "\nA lista possui pera? " + listaFrutas.contains("pera");
		saida += "\nA lista possui caju? " + listaFrutas.contains("caju");
		listaFrutas.remove("uva");
		saida += "\nTotal de Frutas na lista " + "apos remover uva: " + listaFrutas.size();
		saida += "\nFrutas da Lista " + "apos remover uva: " + listaFrutas.toString();
		saida += "\n�ndice da Fruta " + "caju na lista: " + listaFrutas.indexOf("caju");

		JTextArea saidaArea = new JTextArea();
		saidaArea.setText(saida);

		JOptionPane.showMessageDialog(null, saidaArea, "Trabalhando com ArrayList", JOptionPane.INFORMATION_MESSAGE);

		System.exit(0);
	}

}
