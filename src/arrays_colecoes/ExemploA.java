/**
 * 
 */
package arrays_colecoes;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 * @author Thiago da Silva
 *
 */
public class ExemploA {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] array;

		// declara refer�ncia para um array
		array = new int[10];

		// cria um array com 10 elementos
		String saida = "�ndice\tValor\n";

		// adicionando o valor de cada
		// elemento do Array na vari�vel saida
		for (int i = 0; i < array.length; i++) {
			saida += i + "\t" + array[i] + "\n";
		}

		JTextArea saidaArea = new JTextArea();
		saidaArea.setText(saida);
		JOptionPane.showMessageDialog(null, saidaArea, "Criando um Array de Inteiros", JOptionPane.INFORMATION_MESSAGE);

		System.exit(0);
	}
}
