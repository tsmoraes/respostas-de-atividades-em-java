/**Apresenta v�rias telas pedindo que digite os dados do cliente.
 * 
 *  @author Thiago da Silva
 */

package construtor_com_sobrecarga;

import javax.swing.JOptionPane;

public class TestaCliente {

	public static void main(String[] args) {
		// criamos aqui os atributos a serem lidos
		// referente ao cliente 1 e cliente 2
		int codigo;
		String nome;
		String cpf;
		String endereco;
		// aqui lemos os valores dos atributos pelo usuario
		// referente ao primeiro cliente
		codigo = Integer.parseInt(JOptionPane.showInputDialog("Digite o codigo do primeiro cliente:"));
		nome = JOptionPane.showInputDialog("Digite o nome do primeiro cliente");
		cpf = JOptionPane.showInputDialog("Digite o cpf do primeiro cliente");
		endereco = JOptionPane.showInputDialog("Digite o endere�o do primeiro cliente");
		// criamos um objeto do tipo cliente e usamos um
		// construtor vazio, passamos os valores para
		// os atributos depois dele instanciado
		Cliente c1 = new Cliente();
		c1.codigo = codigo;
		c1.nome = nome;
		c1.cpf = cpf;
		c1.endereco = endereco;
		// aqui lemos os valores dos atributos pelo usuaio
		// referente ao segundo cliente
		codigo = Integer.parseInt(JOptionPane.showInputDialog("Digite o codigo do segundo cliente:"));
		nome = JOptionPane.showInputDialog("Digite o nome do segundo cliente");
		cpf = JOptionPane.showInputDialog("Digite o cpf do segundo cliente");
		endereco = JOptionPane.showInputDialog("Digite o endere�o do segundo cliente");
		// aqui ent�o instanciamos o objeto passando para
		// o construtor os valores dos atributos j� neste
		// momento
		Cliente c2 = new Cliente(codigo, nome, cpf, endereco);

	}

}
