/**Responsável por receber os dados do cliente.
 * 
 *  @author Thiago da Silva
 */

package construtor_com_sobrecarga;

public class Cliente {
	// criamos aqui atributos normais de uma classe Cliente
	int codigo;
	String nome;
	String cpf;
	String endereco;

	// criamos aqui um constructor vazio para a classe
	public Cliente() {
	}

	// criamos aqui outro constructor passando valores
	// de entrada para inicializar os atributos
	public Cliente(int codigo, String nome, String cpf, String endereco) {
		this.codigo = codigo;
		this.nome = nome;
		this.cpf = cpf;
		this.endereco = endereco;
	}
}
