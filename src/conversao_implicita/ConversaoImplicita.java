package conversao_implicita;

/**retorna um valor incorreto se receber um parametro que n�o seja do tipo int.
 * 
 *  @author Thiago da Silva
 */

public class ConversaoImplicita {
	int retornaQuadradoNumero(int numero) {
		numero = numero * numero;
		return numero;
	}
}
