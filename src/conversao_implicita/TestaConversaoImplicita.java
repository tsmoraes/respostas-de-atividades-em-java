package conversao_implicita;

/**Se o valor passado no argumento for v�lido pela convers�o impl�cita,
 * o m�todo poder� ser utilizado sem erro de execu��o, mas seus valores 
 * poder�o sofrer altera��es.
 * 
 *  @author Thiago da Silva
 */

import javax.swing.JOptionPane;

public class TestaConversaoImplicita {

	public static void main(String[] args) {
		ConversaoImplicita c1 = new ConversaoImplicita();
		JOptionPane.showMessageDialog(null, "Quadrado de 3= " + c1.retornaQuadradoNumero(3));
		JOptionPane.showMessageDialog(null, "Quadrado de 3= " + c1.retornaQuadradoNumero('3'));

	}

}
