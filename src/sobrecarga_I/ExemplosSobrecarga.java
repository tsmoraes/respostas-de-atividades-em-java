package sobrecarga_I;

/**M�todos respons�veis pela verifica��o do maior n�mero digitado.
 * 
 *  @author Thiago da Silva
 */

public class ExemplosSobrecarga {
	// criamos aqui a vari�vel maior que ser� usada na veriica��o do
	// maior n�mero digitado
	double maior;
	String repetidos;

	// m�todo que veriica o maior entre dois n�meros digitados
	public double calculaNumeroMaior(double n1, double n2) {
		if (n1 == n2) {
			repetidos = "Os mesmos n�meros";
		} else if (n1 > n2) {
			maior = n1;
		} else if (n2 > n1) {
			maior = n2;
		}
		return maior;
	}

	// m�todo que veriica o maior n�mero entre ter n�meros digitados
	public double calculaNumeroMaior(double n1, double n2, double n3) {
		if ((n1 > n2) && (n1 > n3)) {
			maior = n1;
		} else if ((n2 > n1) && (n2 > n3)) {
			maior = n2;
		} else if ((n3 > n1) && (n3 > n2)) {
			maior = n3;
		} else {
			repetidos = "n�meros iguis";
		}
		return maior;
	}

}
