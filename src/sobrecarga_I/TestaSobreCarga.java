package sobrecarga_I;

/**Verifica qual � o maior n�mero utilizando a Sobrecarga de M�todos.
 * 
 *  @author Thiago da Silva
 */

import javax.swing.JOptionPane;

public class TestaSobreCarga {

	public static void main(String[] args) {
		double n1, n2, n3, maior;
		// testando o primeiro metodo
		n1 = Double.parseDouble(JOptionPane.showInputDialog("Digite o primeiro numero:"));
		n2 = Double.parseDouble(JOptionPane.showInputDialog("Digite o segundo numero:"));

		ExemplosSobrecarga e1 = new ExemplosSobrecarga();
		maior = e1.calculaNumeroMaior(n1, n2);
		JOptionPane.showMessageDialog(null, "Maior numero digitado: " + maior);

		// testando o primeiro segundo metodo
		n1 = Double.parseDouble(JOptionPane.showInputDialog("Digite o primeiro numero:"));
		n2 = Double.parseDouble(JOptionPane.showInputDialog("Digite o segundo numero:"));
		n3 = Double.parseDouble(JOptionPane.showInputDialog("Digite o terceiro numero:"));

		ExemplosSobrecarga e2 = new ExemplosSobrecarga();
		maior = e2.calculaNumeroMaior(n1, n2, n3);
		JOptionPane.showMessageDialog(null, "Maior numero digitado: " + maior);
	}
}
