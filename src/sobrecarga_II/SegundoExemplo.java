package sobrecarga_II;

/**M�todos respons�veis por incluir a m�scara no DDD.
 * 
 *  @author Thiago da Silva
 */

public class SegundoExemplo {
	// criamos aqui uma string que sera o ddd formatado
	String dddComMascara;

	// criamos aqui um metodo que retorna o ddd formatado recebendo
	// um DDD to dipo int
	String retornaDddMascara(int ddd) {
		dddComMascara = "(" + ddd + ")";
		return dddComMascara;
	}

	// criamos aqui um m�todo que retorna o ddd formatado recebendo
	// um DDD to dipo string
	String retornaDddMascara(String ddd) {
		dddComMascara = "(" + ddd + ")";
		return dddComMascara;
	}

}
