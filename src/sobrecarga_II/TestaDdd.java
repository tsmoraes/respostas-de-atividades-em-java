package sobrecarga_II;

/**Retorna o DDD formatado com m�scara.
 * 
 *  @author Thiago da Silva
 */

import javax.swing.JOptionPane;

public class TestaDdd {

	public static void main(String[] args) {
		// criamos aqui duas variaveis para ddd uma int oura string
		int ddd;
		String strDdd;
		// vamos ler aqui a variavel ddd que � integer
		ddd = Integer.parseInt(JOptionPane.showInputDialog("Digite o ddd de sua cidade:"));
		// vamos ler aqui a variavel ddd que � string
		strDdd = JOptionPane.showInputDialog("Digite o ddd de sua cidade:");

		SegundoExemplo se = new SegundoExemplo();
		// vamos imprimir primeiro o ddd int com mascara
		JOptionPane.showMessageDialog(null, "DDD int com mascara:" + se.retornaDddMascara(ddd));
		// vamos imprimir agora o ddd string com mascara
		JOptionPane.showMessageDialog(null, "DDD string com	mascara: " + se.retornaDddMascara(strDdd));
	}

}
