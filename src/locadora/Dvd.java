/**Herda a classe ItemAbstrato.
 * 
 * @author Thiago da Silva
 */

package locadora;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Dvd extends ItemAbstrato {
	SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
	
	public Dvd() {
	}
	
	@Override
	public void cadastrar() {
		setCodigo(1);
		setTitulo("Doutor Estranho");
		setSituacaoItem("L");
		System.out.println("DVD Cadastrado: " + " - Cod: " + getCodigo() + " - T�tulo: " + getTitulo() + " - Situa��o: " + getSituacaoItem());
		System.out.println("Exemplo de cadastro de DVD utilizando m�todo abstrato" + "Herdado da classe abstrata ItemAbstrato");
		
	}

	@Override
	public void emprestar() {
		setSituacaoItem("E");
		setDataEmprestimo(sdf.format(new Date()));
		System.out.println("DVD Devolvido: " + " - Situa��o: " + getSituacaoItem() + " - Data da Devolu��o: " + this.getDataDevolucao());
		System.out.println("Devolu��o de DVD com a utiliza��o de m�todo abstrato " + "Herdado da classe abstrata ItemAbstrato");
		
	}

	@Override
	public void devolver() {
		setSituacaoItem("E");
		setDataEmprestimo(sdf.format(new Date()));
		System.out.println("DVD Emprestado: " + " - Situa��o: " + getSituacaoItem() + " - Data do emprestimo: " + this.getDataEmprestimo());
		System.out.println("Exemplo de emprestimo de DVD utilizando m�todo abstrato herdado " + "Herdado da classe abstrata ItemAbstrato");
		
	}
	
	public void imprimir() {
		System.out.println("Lista de DVDs utilizando m�todo" + "concreto da Classe Dvd.");
		
	}
	
	public static Dvd getInstance() {
		return new Dvd();
		
	}

}
