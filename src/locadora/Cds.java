/**
 * Herda a classe Dvd.
 * 
 * @author Thiago da Silva
 */

package locadora;

public class Cds extends Dvd {

	@Override
	public void cadastrar() {
		setCodigo(1);
		setTitulo("Liga da Justi�a");
		setSituacaoItem("L");
		System.out.println("CD Cadastrado: " + " - Cod: " + getCodigo() + " - T�tulo: " + getTitulo() + " - Situa��o: " + getSituacaoItem());
		System.out.println("Exemplo de cadastro de CD utilizando m�todo abstrato" + "Herdado da classe abstrata ItemAbstrato");
		
	}
	
	public void imprimir() {
		System.out.println("Lista de CDs utilizando m�todo" + "concreto da Classe Dvd.");
		
	}
	
	public void vender() {
		System.out.println("Exemplo de venda de CD utilizando m�todo" + "concreto da Classe Cds.");
		
	}
	
	public static Cds getInstance() {
		return new Cds();
		
	}
}
