/**Apresenta um menu com as opc�es chamando os m�todos implementados.
 * 
 * @author Thiago da Silva
 */

package locadora;

import java.util.Scanner;

public class Programa {

	public static void main(String[] args) {
		int opcao; Dvd dvd; Cds cds;
        Scanner entrada = new Scanner(System.in);

        do{
        	System.out.println(" ");
        	System.out.println("Analista Desenvolvedor: Thiago da Silva Moraes");
            System.out.println(" ------------------------- ");
            System.out.println("Digite 0 caso queira sair ou outro n�mero das op��es abaixo: ");
            System.out.println("1 -> Cadastrar DVD " + "2 -> Devolver DVD " + "3 -> Emprestar DVD ");
            System.out.println("4 -> Cadastrar CD " + "5 -> Vender CD " + "6 -> Imprimir Lista CD ");   
            System.out.println("Opcao: ");

            opcao = entrada.nextInt();
            System.out.println(" ------------------------- ");
            System.out.println(" ");
            
            if(opcao == 0)
            	System.exit(0);
            dvd = Dvd.getInstance();
            cds = Cds.getInstance();
            switch(opcao){
            case 1:
                dvd.cadastrar();
                break;
                
            case 2:
                dvd.emprestar();
                break;
                
            case 3:
                dvd.devolver();
                break;
                
            case 4:
                cds.cadastrar();
                break;
            
            case 5:
                cds.vender();
                break;
                
            case 6:
                cds.imprimir();
                break;
                
            default:
                System.out.println("Op��o inv�lida.");
            }
        } while(opcao != 0);

	}

}
