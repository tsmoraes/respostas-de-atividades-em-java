/**
 *  @author Thiago da Silva
 */

package atividade_fornecedor;

public class Fornecedor {
	int codigo;
	String razaoSocial;

	public Fornecedor() {
	}

	public Fornecedor(int codigo, String razaoSocial) {
		this.codigo = codigo;
		this.razaoSocial = razaoSocial;
	}
}
